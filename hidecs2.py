# Python implementations of functions described in:
# Research Report R62-2
# HIDECS 2: A COMPUTER PROGRAM FOR THE HIERARCHICAL DECOMPOSITION
# OF A SET WHICH HAS AN ASSOCIATED LIEAR GRAPH
# by Christopher Alexander, Society of Fellows, Harvard University
# and Marvin L. Manheim, Department of Civil Engineering, M.I.T.
# Publication No. 160, June 1962

import random


def calculate_info(TOTAL, ORDER, RR, M, N):
    NSQ1 = ORDER * (ORDER-1) / 2.0  # The max number of links possible.
    # if TOTAL == NSQ1: # A complete graph. According to SMRMN page 36.
    # 	return float("-inf") #make sure that it will be minimum
    # written slighltly different (page 26)
    denom = RR - M*N * TOTAL/NSQ1

    if M*N == 0:
        raise RuntimeError(f" M*N is 0, with M: {M}, N: {N}")
    if M*N == NSQ1:
        raise RuntimeError(
            f" NSQ1 - M*N is 0, with M: {M}, N: {N},, ORDER: {ORDER}, RR {RR}")

    info = denom*denom/(M*N * (NSQ1 - M*N))
    return info if denom >= 0 else -info  # STR^2 with sign. Page 26


def calculate_total(graph, v_set):  # Number of edges linking points that are both in vSet
    total = 0
    for e in graph:
        total += 1 if e[0] in v_set and e[1] in v_set else 0
    return total


def calculate_RR(graph, v_set, v_subset):
    rr = 0
    for e in graph:
        if e[0] not in v_set or e[1] not in v_set:
            raise RuntimeError(
                "Graph includes links ot nodes outside vSet in calculateRR")
        case_cut = 1 if e[0] in v_subset else 0
        case_cut += 1 if e[1] in v_subset else 0
        rr += 1 if case_cut == 1 else 0
    return rr

# graph, vSet a set of vertices, vSubset as a set of vertices in vSet


def calculate_graph_info(graph, v_set, v_subset):
    order = len(v_set)
    sub_order = len(v_subset)
    rr = calculate_RR(graph, v_set, v_subset)
    total = calculate_total(graph, v_set)
    return calculate_info(total, order, rr, order-sub_order, sub_order)


def genetate_start_of_path(v_set):  # part of SMRMN in Alexander (C8)
    sub_set = set()
    choice_set = list(v_set)
    while not sub_set:  # make sure it is not empty
        for i in range(random.randint(1, len(choice_set)-1)):  # prevent same length
            sub_set.add(random.choice(choice_set))
    return sub_set


def add_loop(graph, v_set, v_sub):
    selected = min([el for el in v_set if el not in v_sub],
                   key=lambda el: calculate_graph_info(graph, v_set, v_sub | {el}))
    return selected if (calculate_graph_info(graph, v_set, v_sub | {selected}) <
                        calculate_graph_info(graph, v_set, v_sub)) else None


def subtract_loop(graph, v_set, v_sub):
    selected = min(v_sub, key=lambda el: calculate_graph_info(
        graph, v_set, v_sub - {el}))
    return selected if (calculate_graph_info(graph, v_set, v_sub-{selected}) <
                        calculate_graph_info(graph, v_set, v_sub)) else None


# vSub is the TSET in Alexander's description.
def hill_climb(graph, v_set, v_sub):
    while True:
        to_add = add_loop(graph, v_set, v_sub) if len(
            v_sub) < len(v_set) - 1 else None
        if to_add:
            v_sub.add(to_add)

        to_subtract = subtract_loop(graph, v_set, v_sub) if len(v_sub) > 1 else None
        if to_subtract:
            v_sub.remove(to_subtract)

        if not to_add and not to_subtract:  # if it could not improve
            break
    return v_sub


# run the hillclimber LATIS times, and choose best result
def SMRMN(graph, v_set, LATIS):
    # check if graph (vset) is complete:
    total = calculate_total(graph, v_set)
    order = len(v_set)
    nsq1 = order * (order-1) / 2.0
    if total == nsq1:  # is complete
        return v_set

    if order == 2:
        return {v_set.pop()}  # since it has two elements any would do.
    t = 0
    info = float('inf')  # a max value
    v_sub = []
    while t < LATIS:
        nv_sub = genetate_start_of_path(v_set)
        nv_sub = hill_climb(graph, v_set, nv_sub)
        n_info = calculate_graph_info(graph, v_set, nv_sub)

        if n_info < info:
            v_sub = nv_sub
            info = n_info
        t += 1

    return v_sub


def sub_graph(graph, v_set):
    return [e for e in graph if e[0] in v_set and e[1] in v_set]


# addBranch has to return a node.
def subdivide(graph, v_set, attach, make_node, make_leaf):

    def subdivide(graph, vSet):
        graph = sub_graph(graph, vSet)  # getSubgraph, optimisation
        # subSet = SMRMN(graph,vSet,10)
        # scaling of LATIS depending on size
        sub_set = SMRMN(graph, vSet, 2*len(vSet)+10)
        if sub_set == vSet:
            return make_leaf(sorted(vSet))  # to print them sorted

        complement = set([el for el in vSet if el not in sub_set])

        node = make_node()
        attach(node, subdivide(graph, sub_set))
        attach(node, subdivide(graph, complement))

        return node

    if v_set is not set:
        v_set = set(v_set)

    return subdivide(graph, set(v_set))
