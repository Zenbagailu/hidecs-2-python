import json  # to open and save data as json


def graph_from_adjacencies(adjacencies):
    edgeMap = {}
    for adj in adjacencies:
        v0 = adj[0]
        for v1 in adj[1]:

            # edge with smallest index first
            if (v0 > v1):
                va, vb = v1, v0
            else:
                va, vb = v0, v1
            edge = (va, vb)

            if va not in edgeMap:
                # the first is used to store "one directional edges," those
                # only present in one of the nodes (they need to be removed)
                # the second stores the complete list
                edgeMap[va] = [[], []]

            alreadyAdded = False
            for e in edgeMap[va][0]:
                if e == edge:
                    alreadyAdded = True
                    # this leaves an
                    # edgeMap with all links that are not
                    # represented in both sides
                    edgeMap[va][0].remove(edge)
                    break

            if not alreadyAdded:
                edgeMap[va][0].append(edge)
                edgeMap[va][1].append(edge)

    for el in edgeMap:
        # remove dangling edges
        for e in edgeMap[el][0]:
            edgeMap[el][1].remove(e)

    edges = []
    for el in edgeMap:
        for e in edgeMap[el][1]:
            edges.append(e)

    vertices = []
    for adj in adjacencies:
        vertices.append(adj[0])

    return (vertices, edges)

# These are for making presentation easy

# def to_data():
# 	return dataFromAdjacencies(adjacencyList)


def to_json(file_name, requirements, adjacencies):
    if not file_name.endswith('.json'):
        file_name = f"{file_name}.json"

    data = {"requirements": requirements, "adjacencies": adjacencies}
    with open(file_name, 'w', encoding='utf-8') as f:
        json.dump(data, f, ensure_ascii=False, indent=4)


def from_json(fileName):
    with open(fileName) as dataFile:
        data = json.load(dataFile)
        if "requirements" not in data or "adjacencies" not in data:
            print('json file was not properly formated, it lacked "requirements" or "adjacencies".')

    return data["requirements"], data["adjacencies"]


def print_out(requirements, adjacencies):
    print()
    print("*****************************************************************************")
    print("REQUIREMENTS:")
    print("*****************************************************************************")
    print()

    ct = 0
    for req in requirements:
        ct += 1
        print(f"{ct}\t{req}")

    print("*****************************************************************************")
    print("RELATIONS:")
    print("*****************************************************************************")
    print()
    for adj in adjacencies:
        print(f"{adj[0]}:\t{adj[1]}")

    print()
    print("*****************************************************************************")
