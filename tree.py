
def new_node():
    return ['n', new_leaf(""), new_leaf("")]


def new_leaf(val):
    return ['l', val]


def set_child_a(node, elem):
    node[1] = elem


def set_child_b(node, elem):
    node[2] = elem


# check if it has default value or not
# it is not set if the chid is a leaf and the leaf is an empty string

def is_stump(node):
    return is_node_leaf(node) and not get_leaf(node)


def has_child_a(node):
    return not is_stump(node[1])


def has_child_b(node):
    return not is_stump(node[2])


def add_child(node, elem):  # a practical way of not caring for a or b
    if not has_child_a(node):
        set_child_a(node, elem)
    elif not has_child_b(node):
        set_child_b(node, elem)
    else:
        raise RuntimeError("Trying to add a child to a filled node.")


def is_node_leaf(node):
    return node[0] == 'l' if node else False


def get_child_a(node):
    if node:
        return node[1]
    return None


def get_child_b(node):
    if node:
        return node[2]
    return None


def get_leaf(node):  # it works only if it is a leaf.
    return node[1]


# Draw --------------------------------------
# A basic drawing/printing for tests. More complex forms of drawing
# Are given in tree_drawing.py

def draw_node(node, depth):
    if node and len(node) <= 1:
        print("empty")
        return
    print('\t'*depth + '[')

    if is_node_leaf(node):
        print('\t'*depth + str(get_leaf(node)))
        return
    elif is_node_leaf(node):
        print('\t'*depth + node[1])
    else:
        draw_node(get_child_a(node), depth + 1)
        draw_node(get_child_b(node), depth + 1)

    print('\t'*depth + ']')


def draw_tree(tree):
    draw_node(tree, 0)
